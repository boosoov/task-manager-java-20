package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

public class NotExistAbstractListException extends RuntimeException {

    public NotExistAbstractListException() {
        super("Error! Abstract list is not exist...");
    }

}
