package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.logic.InternalProgramException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProfileShowCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "view-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Show profile.";
    }

    @Override
    public void execute() {
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.getById(userId);
        System.out.println(user);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN, Role.USER};
    }

}
