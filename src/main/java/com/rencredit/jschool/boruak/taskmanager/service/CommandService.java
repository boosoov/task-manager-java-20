package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyNameException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Objects;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    public Map<String, AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @NotNull
    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    @NotNull
    public String[] getArgs() {
        return commandRepository.getArgs();
    }

    public void putCommand(@Nullable final String name, @Nullable final AbstractCommand command) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (command == null) throw new EmptyCommandException();
        commandRepository.putCommand(name, command);
    }

}
