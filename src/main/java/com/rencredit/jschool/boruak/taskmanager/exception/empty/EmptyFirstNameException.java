package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyFirstNameException extends RuntimeException {

    public EmptyFirstNameException() {
        super("Error! First name is empty...");
    }

}
