package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyCommandException extends RuntimeException {

    public EmptyCommandException() {
        super("Error! Command is empty...");
    }

}
