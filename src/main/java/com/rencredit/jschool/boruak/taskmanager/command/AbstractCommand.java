package com.rencredit.jschool.boruak.taskmanager.command;

import com.rencredit.jschool.boruak.taskmanager.api.repository.*;
import com.rencredit.jschool.boruak.taskmanager.api.service.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.repository.*;
import com.rencredit.jschool.boruak.taskmanager.service.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public abstract class AbstractCommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract  String description();

    public abstract void execute() throws IOException, ClassNotFoundException;

}
