package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.rencredit.jschool.boruak.taskmanager.api.service.IDomainService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IProjectService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DataBinaryLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data to binary file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final ObjectInputStream objectInputStream;
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
        objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.load(domain.getProjects());
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.load(domain.getTasks());
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.load(domain.getUsers());
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
