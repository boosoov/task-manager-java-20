package com.rencredit.jschool.boruak.taskmanager.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Setter
@Getter
@NoArgsConstructor
public class Command {

    @NotNull private String name = "";

    @NotNull private String arg = "";

    @NotNull private String description = "";

    public Command(@NotNull final String name, @NotNull final String arg, @NotNull final String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public Command(@NotNull final String name, @NotNull final String arg) {
        this.name = name;
        this.arg = arg;
    }

    public Command(@NotNull final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        if (!name.isEmpty()) result.append(name);
        if (!arg.isEmpty()) result.append(", ").append(arg);
        if (!description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }

}
