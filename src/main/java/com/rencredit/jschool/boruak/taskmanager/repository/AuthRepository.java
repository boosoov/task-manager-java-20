package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IAuthRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AuthRepository implements IAuthRepository {

    private String userId;

    @Nullable
    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void logIn(@NotNull final User user) {
        userId = user.getId();
    }

    @Override
    public void logOut() {
        userId = null;
    }

}
