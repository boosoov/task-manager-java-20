package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.*;
import com.rencredit.jschool.boruak.taskmanager.api.service.*;
import com.rencredit.jschool.boruak.taskmanager.repository.*;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public class ServiceLocator implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final IAuthService authService = new AuthService(userService, authRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(projectService, taskService, userService);

}
