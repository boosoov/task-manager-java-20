package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void clearAll();

    public boolean addRecord(@NotNull E record);

    public boolean removeRecord(@NotNull E record);

    @NotNull
    List<E> getList();

    void load(@NotNull Collection<E> elements);

    void load(@NotNull E... elements);

    @NotNull
    E merge(@NotNull E element);

    void merge(@NotNull Collection<E> elements);

    void merge(@NotNull E... elements);

}
