package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    private final List<E> records = new ArrayList<>();

    public boolean addRecord(@NotNull E record) {
        return records.add(record);
    }

    public boolean removeRecord(@NotNull E record) {
        return records.remove(record);
    }

    @NotNull
    public  List<E> getList() {
        return records;
    }

    public void clearAll() {
        records.clear();
    }

    public void load(@NotNull final Collection<E> projects) {
        clearAll();
        merge(projects);
    }

    public void load(@NotNull final E... records) {
        clearAll();
        merge(records);
    }

    @NotNull
    public E merge(@NotNull final E record) {
        records.add(record);
        return record;
    }

    public void merge(@NotNull final Collection<E> records) {
        for (@NotNull final E record : records) merge(record);
    }

    public void merge(@NotNull final E... records) {
        for (@NotNull final E record : records) merge(record);
    }

}
