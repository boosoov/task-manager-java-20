package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.rencredit.jschool.boruak.taskmanager.api.service.IDomainService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA XML LOAD]");
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML)));
        @NotNull final Domain domain = xmlMapper.readValue(xml, Domain.class);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
