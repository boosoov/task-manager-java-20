package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.dto.Command;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public interface ICommandRepository {

    @NotNull
    Map<String, AbstractCommand> getTerminalCommands();

    @NotNull
    String[] getCommands();

    @NotNull
    String[] getArgs();

    void putCommand(@NotNull String name, @NotNull AbstractCommand command);

}
