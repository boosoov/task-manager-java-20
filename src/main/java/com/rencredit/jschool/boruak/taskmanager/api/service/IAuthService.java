package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    @Nullable
    String getUserId();

    void checkRoles(@Nullable Role[] roles);

    void logIn(@Nullable String login, @Nullable String password);

    @NotNull
    User registration(@Nullable String login, @Nullable String password);

    @NotNull
    User registration(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User registration(@Nullable String login, @Nullable String password, @Nullable Role role);

    void logOut();

}
